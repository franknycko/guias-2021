#!/usr/bin/env python
#! -- coding: utf-8 --

''' 4. Se requiere un programa para determinar cuanto
       ahorrara una persona en un año, si al final de
       cada mes deposita variables cantidades de dinero;
       ademas, se requiere saber cuanto lleva ahorrado cada mes.'''
#Varable en el que le ire colocando los depositos.
ahorrado = 0
#Ciclo for ya que se sabe la cantidad de meses que se requieren.
for i in range(1, 13):
    #Se pregunta dentro del for para que se respita constantemente.
    deposito = int(input("Indique cuanto es lo que desea depositar: "))
    ahorrado = ahorrado + deposito
    #Indico cuanto lleva y en que mes va.
    print("Tiene", ahorrado,"ahorrado en el mes", i)
    print("")#Para que no se vea muy junto.
