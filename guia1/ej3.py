#!/usr/bin/env python
#! -- coding: utf-8 --

''' 3. Una multitienda tiene una promocion: a todos los trajes
       que tienen un precio superior a $10000 se les aplicara un
       descuento de 15%, a todos los demas se les aplicara solo 8%.
       Realice un programa para determinar el precio final que debe pagar
       una persona por comprar un traje y de cuanto es el
       descuento que obtendra. Pida por lo menos ingresar 3 elementos.'''

print("      Porfavor ingrese los precios de sus trajes")
#Variable que cambiara constantemente.
descuento = 0
#Ingresion del primer traje
a = int(input("Primer producto: "))
#Condiciones para saber que descuento tendra.
if a > 10000:
    descuento = a * 0.15
    print("Su descuento de 15% es de: $", descuento)
    print("El precio de su traje es de: $", a - descuento)
else:
    descuento = a * 0.08
    print("Su descuento de 8% es de: $", descuento)
    print("El precio de su traje es de: $", a - descuento)
print("")#Espacio entre los calculos de traje para que no se vea tan junto.
#Ingresion de segundo traje.
b = int(input("Segundo producto: "))
#Condiciones para segundo  traje
if b > 10000:
    descuento = b * 0.15
    print("Su descuento de 15% es de: $", descuento)
    print("El precio de su traje es de: $", b - descuento)
else:
    descuento = b * 0.08
    print("Su descuento de 8% es de: $", descuento)
    print("El precio de su traje es de: $", b - descuento)
print("")#Espacio para que no se vea tan junto.
#Ingresion de tercer traje.
c = int(input("Tercer producto: "))
#Condiciones del tercer traje.
if c > 10000:
    descuento = c * 0.15
    print("Su descuento de 15% es de: $", descuento)
    print("El precio de su traje es de: $", c - descuento)
else:
    descuento = c * 0.08
    print("Su descuento de 8% es de: $", descuento)
    print("El precio de su traje es de: $", c - descuento)
