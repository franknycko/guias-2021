#!/usr/bin/env python
#! -- coding: utf-8 --

''' 10. Para resolver este problema se debe identificar
    que la forma A esta compuesta por dos figuras:
    untriangulo de base B y de altura (A - C); y por otro lado,
    un rectangulo que tiene base B y altura C
    y la suma de ambas  ́areas es el resultado final.'''
#Pedir los datos necesarios.
a = int(input("Lado A: "))
b = int(input("Lado B: "))
c = int(input("Lado C: "))
#Condicion para que sea positivo.
if a > 0 and b > 0 and c > 0:
    #Condicion para que el ladoc no sea igual o mayor al lado a, ya que seria una figura diferente.
    if a <= c:
        print("El lado c no puede ser igual o mas grande que el lado a")
    #Ahora si entra a calcularlo.
    else:
        areatr = ((a-c) * b) / 2
        arear = b * c
        areat = areatr + arear
        print("El area total de la figura es de:", areat)
#Muestra que no es valido un dato.
else:
    print("Uno de los datos ingresado ha sido negativo")
