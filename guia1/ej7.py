#!/usr/bin/env python
#! -- coding: utf-8 --

''' 7. “El buen gordito” ofrece hamburguesas sencillas, dobles y triples,
       las cuales tienen un costo de $2000, $2500 y $3000 respectivamente.
       La empresa acepta tarjetas de credito con un cargo de 5% sobre la compra.
       Suponiendo que los clientes adquieren solo un tipo de hamburguesa,
       realice un algoritmo para determinar cuanto debe pagar
       una persona por N hamburguesas.'''

#explico cuales son las opciones.
a = int(input("Si decea la hamburguesa sencilla coloque '1', si desea la doble maque '2', y si desea la triple marque '3': "))
compra = 0
cargo = 0
#Las hamburguesa simple, doble y triple, les doy el valor 1, 2 y 3 respectivamente.
if a == 1:
    n = int(input("Ingrese cuantas quiere: "))
    #Uso las variles pra calcularlas.
    compra = n * 2000
    #Calculo del 5% de la tarjeta.
    cargo = compra * 0.05
    #En la misma impresion hago la suma del 5% calculado anteriormente.
    print("El total de su compra de hamburguesa sencilla es de:$", cargo + compra)
#Mismo proceso que el anterior if pero con diferente precio.
elif a == 2:
    n = int(input("Ingrese cuantas quiere: "))
    compra = n * 2500
    cargo = compra * 0.05
    print("El total de su compra de hamburguesa doble es de:$", cargo + compra)
#Mismo proceso del anterioriores pero con otro precio.
elif a == 3:
    n = int(input("Ingrese cuantas quiere: "))
    compra = n * 3000
    cargo = compra * 0.05
    print("El total de su compra de hamburguesa triple es de:$", cargo + compra)
#Decir que no eligio ninguna opcion.
else:
    print("No selecciono ninguna hamburguesa")
