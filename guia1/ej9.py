#!/usr/bin/env python
#! -- coding: utf-8 --

''' 9. Para resolver este problema es necesario conocer
       las coordenadas de cada punto (X, Y), y con esto
       poder obtener el cateto de abscisas (X1 y X2)
       y el de ordenadas (Y1 e Y2), y mediante estos valores obtener
       la distancia entre P1 y P2, utilizando el teorema de Pitagoras.'''

#Los ordene por puntos al pedirlos.
print("        Ingrese las coordenadas del primer punto")
x1 = int(input("Coordenada x: "))
y1 = int(input("Coordenada y: "))
print("        Ingrese las coordenadas del segundo punto")
x2 = int(input("Coordenada x: "))
y2 = int(input("Coordenada y: "))
#Saque la diferencia entre los puntos
catx = x2 - x1
caty = y2 - y1
#No importa el signo de estos debido a que se elevan a 2.
distancia = ((catx**2) + (caty**2)) ** 0.5
redondeo = round(distancia, 4)#Redondeado para que no sea tan largo.
#Lo muestro.
print("La distancia entre los dos puntos es de:", redondeo)
