#!/usr/bin/env python
#! -- coding: utf-8 --

''' 1. Realice un programa en Python para determinar cuanto
       se debe pagar por equis cantidad de lapices considerando que
       si son 1000 o mas el costo es de 85 pesos; de lo contrario,
       el precio es de 90 pesos.'''

#Se pide que ingrese la cantidad de lapices.
l = int(input("Cuantos lápices desea: "))

#Defino una variable para poder utilizar la variable "l".
precio = 0

#Condicion necesaria para poder utilizar la variable "l".
if l >= 1000:
    precio = l * 85
    print("El precio por", l ,"lápices es de:", precio)
else:
    precio = l * 90
    print("El precio por", l ,"lápices es de:", precio)
