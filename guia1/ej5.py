#!/usr/bin/env python
#! -- coding: utf-8 --

''' 5. Una empresa que contrata personal requiere determinar
       la edad de cada una de las personas que solicitan trabajo,
       pero cuando se les realiza la entrevista solo se les pregunta
       el año en que nacieron. Tambien se requiere el
       promedio de edad de las personas contratadas.'''
#Declare las variables que necesite.
respuesta = None
fechas = 0
vueltas = 0
sum = 0
#Un while debido a que vi que es un ejercicio que no esta definido en cuantas personas seran.
while respuesta != "n":#Dejo la 'n' como termino del proceso.
    #Fehas de nacimientos
    fechas = int(input("Ingrese su fecha de nacimiento: "))
    #Les coloque unas condiciones para hacer que sean un poco realista la edad de estos para un trabajo.
    if fechas > 1920 and fechas < 2012:
        edad = 2021 - fechas
        #Les voy diciendo la edad que les calcule.
        print("Su edad es:", edad)
        #Esta variable va sumando las edades para el promedio.
        sum = sum + edad
        #Esta variable va sumando las vueltas del while para dividir y hacer el promedio.
        vueltas = vueltas + 1
        #Pregunto si es que el usuario quiere colocar mas fechas de nacimiento.
        respuesta = input("Si quiere ingresar mas fechas aprete enter y si quiere terminar escriba 'n':")
    #Les dejo un mensaje para decir que la fecha de nacimiento no es muy realista.
    else:
        print("              Al parecer coloco mal su fecha de nacimiento, favor de intentarlo de nuevo")
#Finalmente muestro el promedio.
print("El promedio de las edades son: ", sum / vueltas, "años")
