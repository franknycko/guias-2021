#!/usr/bin/env python
#! -- coding: utf-8 --

''' 2. Inicialice una variable en None y si la palabra ingresada
       es “Hola” debe imprimir Chao, caso contrario debe decir,
       no entiendo tu mensaje e imprimir el valor de la variable.'''

#Inicializada y rellenada la variable.
a = None
a = input("Ingrese una palabra: ")

#Condicion para que diga "Chao".
if a == "Hola":
    print("Chao")
#El otro caso para decir que no se entiende el mensaje.
else:
    print("No entiendo tu mensaje:", a)
