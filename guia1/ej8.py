#!/usr/bin/env python
#! -- coding: utf-8 --

''' 8. Por lo tanto, para poder resolver el problema,
       se tiene que calcular el cateto faltante, que es la altura
       del triangulo, con esta se puede calcular el  ́area del triangulo,
       y para obtener el area total triangularse multiplicara por dos.
       Por otro lado, para calcular el area de la parte circular,
       se calcula el ́area de la circunferencia y luego se divide entre dos,
       ya que representa solo la mitad del cırculo.'''

#Pido los unicos datos necesarios para resolver.
radio = float(input("Ingrese el radio: "))
hip = float(input("Ingrese la hipotenusa: "))
pi = 3.1416#Lo declare pi solo por gusto.
#Condicion de que sean positivos.
if radio > 0 and hip > 0:
    #Calculo del cateto para el triangulo.
    cateto = ((hip**2) - (radio**2)) ** 0.5
    #Calcule cada area.
    arear = (radio*cateto) / 2
    areac = pi * (radio**2)
    #Sume las areas.
    areat = (areac/2) + (arear*2)
    #Lo redondeo para que no sea tan grande el numero.
    redondeo = round(areat, 4)
    print("El area total de la figuera es:", redondeo)
#Digo que su numero no es positivo.
else:
    print("Los datos deben ser positivos")
