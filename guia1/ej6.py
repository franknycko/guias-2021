#!/usr/bin/env python
#! -- coding: utf-8 --

''' 6. Un estacionamiento requiere determinar el cobro que debe aplicar
       a las personas que lo utilizan. Considere que el cobro
       es con base en las horas que lo disponen y que las fracciones
       de hora se toman como completas. Cree un programa que
       realice el cobro del estacionamiento.'''

print("El cobro por hora de estacionamiento es de $500")
#Se pide cuanto ha sido el tiempo en horas y minutos.
timeh = int(input("Agregue las horas que ha estado:"))
timem = int(input("Ahora los minutos ha estado: "))
cobro = 0

#Condicion para que sea positivo las horas.
if timeh > 0:
    #Intervalo de minutos que no deberia usarse.
    if timem < 0 or timem >= 60:
        print("Porfavor colocar minutos reales")
    #Si no hay minutos solo se calcula las horas.
    elif timem == 0:
        cobro = timeh * 500
        print("Su tiempo de estacionamiento vale: $", cobro)
    #Es cuando si hay algun minutos ademas de la hora se le agrega la siguiente hora completa.
    else:
        cobro = (timeh + 1) * 500
        print("Su tiempo de estacionamiento vale: $", cobro)
#Decir que hora negativas no existen.
elif timeh < 0:
    print("Porfavor colocar horas reales")
#Indicar cantidad de minutos que existan.
elif timem < 0 or timem >= 60:
    print("Porfavor colocar minutos reales")
#Si no hay horas, se contara de inmediato los minutos como una hora completa.
else:
    print("Tiempo de estacionamiento: 500")
