#!/usr/bin/env python
#! -- coding: utf-8 --

''' 4. Extienda el programa anterior para logre encontrar palabras
       dentro de un texto, sin importar si estan escritas
       en diferente combinacion de mayusculas/minusculas.'''

print("\tTexto elegido")
print("""Escriba un programa que indique si una palabra existe dentro de un texto\n""")
#Defino el texto.
txt = ("Escriba un programa que indique si una palabra existe dentro de un texto")
#Pido la palabra.
pal = str(input("Cual palabra busca: "))
#Condicion para encontrar la palabra en el texto.
if pal in txt:
    print("Si se encontro la palabra:", pal)
else:
    print("No se encuentra la palabra en el texto")
