#!/usr/bin/env python
#! -- coding: utf-8 --

''' 6. Cree un programa que permita concatenar los datos o palabras
       ingresadas por un usuario. El ingreso de palabras debe estar
       controlado por un ciclo y cuando se escriba la palabra fin
       termine la ejecucion y se muestren todos los datos
       ingresados de forma concatenada.'''
#Variables que se necesitan declararse.
resp = None
concatenar = ""
#Bucle que termine solo con la palabra "fin".
while resp != "fin":
    #Se pide constantemente las palabras.
    pal = str(input("Ingrese la palabra que quiere concatenar: "))
    #Aqui se van concatenando con el signo de suma.
    concatenar = concatenar + pal
    #Aqui pido fin para que termine el proceso.
    ter = input("Si desea terminar de concatenar escriba fin sino aprete enter: ")
    #En caso de que haya alguna mayuscula.
    resp = ter.lower()
#Solo queda mostra.
print("Su palabra concatenada es:", concatenar)
