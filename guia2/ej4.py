#!/usr/bin/env python
#! -- coding: utf-8 --

''' 3. Escriba un programa que indique si una palabra
       existe dentro de un texto. La palabra como la
       oracion seran ingresadas por el usuario.'''

print("\tTexto elegido")
print("""Escriba un programa que indique si una palabra existe dentro de un texto\n""")

txt = ("""Escriba un programa que indique si una palabra existe dentro de un texto""")
#Lo acomode en minuscula para encontra de igual modo la palabra.
txt_lower = txt.lower()
#Pido la palabra y tambien la dejo en minuscula.
pal = str(input("Cual palabra busca: "))
pal_lower = pal.lower()
#Condicion para encontrar la palabra en el texto.
if pal_lower in txt_lower:
    print("Si se encontro la palabra:", pal)
else:
    print("No se encuentra la palabra en el texto")
