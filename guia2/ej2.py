#!/usr/bin/env python
#! -- coding: utf-8 --

''' 2. El siguiente String esta encriptado de mala manera
       a pesar que de todos modos sigue siendo un mensaje
       incomprensible. Idea un metodo para extraer el mensaje
       y guardalo en una variable llamada mensaje y mostrar
       el valor de la variable en pantalla.
       string =¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt” '''

men_sec = ("¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt")
#Saque las "X" las que molestaban.
mensaje = men_sec.replace("X", "")
#Inverti completamente el mensaje.
print("".join(reversed(mensaje)))
