#!/usr/bin/env python
#! -- coding: utf-8 --

''' 1. Dada un String que se forman de manera aleatoria,
       devuelva una nueva lista cuyo contenido sea
       igual a la original pero de modo invertida.'''

text = input("Escriba una frase: ")

if text is "":
    print("No ha escrito nada")
else:
    #Lo dividi en palabras
    rev = text.split()
    #Use reversed lo cual lo invertia pero ahora lo tenia separado en palabras
    print(" ".join(reversed(rev)))
