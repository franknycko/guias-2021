#!/usr/bin/env python
#! -- coding: utf-8 --

''' 5. Escriba un programa que pida al usuario dos palabras,y se
       indique cual de ellas es la mas larga y por cuantas letras lo es'''
#Ingresion de las palabras.
pal_1 = str(input("Escriba su palabra: "))
pal_2 = str(input("Escriba su segunda palabra: "))
#Conteo de letras.
num_1 = len(pal_1)
num_2 = len(pal_2)
#Condicion si la primera palabra es mayor y si lo es se ve la diferencia y se imprime.
if num_1 > num_2:
    dif = num_1 - num_2
    print("La palabra", pal_1,"es mas larga que", pal_2,"y tiene", dif,"letras mas")
#Condicion si la segunda palabra es mayor y si lo es se ve la diferencia y se imprime.
elif num_2 > num_1:
    dif = num_2 - num_1
    print("La palabra", pal_2,"es mas larga que", pal_1,"y tiene", dif,"letras mas")
#Condicion si son iguales y solo se indica que lo son.
elif num_1 == num_2:
    print("Tienen la misma cantidad de letras")
#Lo deje si hay alguna condicion que no haya pensado, porque no se me ocurrio alguna.
else:
    print("lmao")
